class Board
  def initialize arr=[]
    @cells = arr.take(9)

    (9 - @cells.length).times { @cells.push nil }
  end


  def set_cell index, val
    if @cells[index].nil?
      @cells[index] = val
      return true
    else
      return false
    end
  end


  def open?
    return @cells.include? nil
  end


  def draw vis=true
    3.times do |i|
      3.times do |j|
        index = (i*3)+j
        cell = @cells[index]
        if cell.nil?
          print " \e[5m#{index + 1}\e[0m " if vis
          print "   " if not vis
        else
          print " #{cell} "
        end
        print "|" unless j>=2
      end
      puts "\n"
      puts "---+---+---" unless i>=2
    end
  end


  def check_win? x,y,z
    if @cells[x] == @cells[y] && @cells[y] == @cells[z]
      return @cells[x]
    else
      return nil
    end
  end


  def winner?
    possibles = [
      [0, 1, 2], # Horizontals
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6], # Verticals
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8], # Diagonals
      [2, 4, 6]
    ]

    possibles.each do |p|
      result = self.check_win? *p
      return result unless result.nil?
    end

    return nil
  end
end

